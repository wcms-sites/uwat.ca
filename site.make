core = 7.x
api = 2

; NOTE: uwat.ca may have customizations in the database, due to the was it was set up.
; This file only records modules that exist and does not guarantee a "from scratch" install will work.

; ShURLy
projects[shurly][type] = "module"
projects[shurly][download][type] = "git"
projects[shurly][download][url] = "https://git.uwaterloo.ca/drupal-org/shurly.git"
projects[shurly][download][tag] = "7.x-1.2-uw_wcms1"

; uWaterloo ShURLy configuration
projects[uw_cfg_shurly][type] = "module"
projects[uw_cfg_shurly][download][type] = "git"
projects[uw_cfg_shurly][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_shurly.git"
projects[uw_cfg_shurly][download][tag] = "7.x-1.1"